<?php get_header(); ?>
  <body>
   <?php get_template_part( 'nav' );?>
    <div class="row">
      <section class="large-8 columns">

        <?php if (have_posts()) {
                while (have_posts()) { 
                  the_post(); 
                    get_template_part( 'content', get_post_format() );?>
        <?php } 
            }else{ ?>
          <h2>Nenhum post</h2>
        <?php } ?>
        <?php get_template_part( 'pagination' );?>
      </section>
    <?php get_sidebar(); ?>
    </div>
<?php get_footer(); ?>