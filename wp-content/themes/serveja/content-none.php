
 <article>          
          <div class="entry-container box">
            <header>
              <h1><a href="#" rel="bookmark">Nada encontrado</a></h1>
            </header>
            <div class="entry-content">
            	<?php if ( is_home() && current_user_can( 'publish_posts' ) ) { ?>

				<p><?php printf( __( 'Ready to publish your first post? <a href="%1$s">Get started here</a>.', 'twentyfourteen' ), admin_url( 'post-new.php' ) ); ?></p>

				<?php }elseif ( is_search() ) { ?>

				<p>Desculpe, mas nada foi encontrado na sua busca. Por favor, tente novamente com algumas palavras diferentes.</p>
				<?php get_search_form(); ?>

				<?php }else{ ?>

				<p><?php _e( 'It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help.', 'twentyfourteen' ); ?></p>
				<?php get_search_form(); ?>

				<?php } ?>
            </div>
          </div>
        </article>
