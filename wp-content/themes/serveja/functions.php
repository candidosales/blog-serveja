<?php
require ('helpers.php'); 


	// Add RSS feed links to <head> for posts and comments.
	add_theme_support( 'automatic-feed-links' );

	add_theme_support('post-thumbnails');
	add_image_size( 'post-thumb-1', 750, 422, true );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list',
	) );

	/*
	 * Enable support for Post Formats.
	 * See http://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside', 'image', 'video', 'audio', 'quote', 'link', 'gallery',
	) );
 

if (function_exists('register_sidebar')) {
	register_sidebar(array(
	'before_widget' => '<div class="box">',
	'after_widget' => '</div>',
	'before_title' => '<h3 class="widget-head">',
	'after_title' => '</h3>',
	));
}

add_editor_style( 'css/dist/editor-style.css' );

