        <footer>
      <div class="row">
        <div class="large-4 columns">
        	Delivery de produtos de convêniencia na madrugada |
        </div>
        <div class="large-4 columns">
          <h3 class="widget-head">Tags</h3>
          <?php wp_tag_cloud(); ?>
        </div>
        <div class="large-4 columns"></div>
        <h3 class="widget-head">SIGN UP TO OUR NEWSLETTER!</h3>
      </div>
      <div class="row-fluid" id="footer-bottom">
         &copy; <?php echo date("Y"); bloginfo('name'); ?> · blog.serveja.com   
    </div>

    </footer>
    <script src="<?php bloginfo('template_url'); ?>/js/dist/main.min.js"></script>
    <?php wp_footer(); ?>
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-15472644-34', 'serveja.com');
      ga('require', 'displayfeatures');
      ga('send', 'pageview');

    </script>
  </body>
</html>

