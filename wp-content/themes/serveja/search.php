<?php get_header(); ?>
<body>
 <?php get_template_part( 'nav' );?>
 <div class="row main">
  <section class="large-8 columns">

    <article <?php post_class() ?> id="post-<?php the_ID(); ?>">
      <div class="entry-container box">
        <header class="page-header">
          <h4 class="page-title">Resultados para: <?php echo get_search_query(); ?></h4>
        </header><!-- .page-header -->
      </div>
    </article>

    <?php 
    if ( have_posts() ) {
      while (have_posts()) { 
        the_post(); 
        get_template_part( 'content', get_post_format() );
        ?>

        <?php }

      }else{ 

        get_template_part( 'content', 'none');

      } ?>
      <?php get_template_part( 'pagination' );?>
    </section>
    <?php get_sidebar(); ?>
  </div>
  <?php get_footer(); ?>
