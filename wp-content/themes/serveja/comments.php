<?php comment_form(); ?>

<?php if ( have_comments() ) { ?>

	<h2 class="comments-title">
		<?php
			printf( _n( '1 comentário', '%1$s comentários', get_comments_number() ),
				number_format_i18n( get_comments_number() ), get_the_title() );
		?>
	</h2>
<ol class="comment-list">
		<?php
			wp_list_comments( array(
				'style'      => 'ol',
				'short_ping' => true,
				'avatar_size'=> 34,
			) );
		?>
	</ol><!-- .comment-list -->

	<?php if ( ! comments_open() ) { ?>
		<p class="no-comments"><?php _e( 'Comments are closed.'); ?></p>
	<?php } ?>

	<?php } // have_comments() ?>