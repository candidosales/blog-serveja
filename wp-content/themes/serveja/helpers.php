<?php

function share_social_media($permalink = null, $title = null ){
	
	echo  '<div class="share-story-container">
                    <h4>Compartilhe</h4>
                    <ul class="share-story">
                      <li><a rel="nofollow"  class="tips" data-title="Facebook" href="http://www.facebook.com/sharer.php?u='.$permalink.'&amp;t='.$title.'" title=""><i class="icon-facebook"></i></a></li>
                      <li><a rel="nofollow"  class="tips" data-title="Google+" href="https://plus.google.com/share?url='.$permalink.'" title=""><i class="icon-googleplus"></i></a></li>
                      <li><a rel="nofollow"  class="tips" data-title="Twitter" href="http://twitter.com/intent/tweet?url='.$permalink.'&amp;text='.$title.'" title=""><i class="icon-twitter"></i></a></li>
                      <li><a rel="nofollow"  class="tips" data-title="Pinterest" href="http://www.pinterest.com/pin/create/button/?url='.$permalink.'" title=""><i class="icon-pinterest"></i></a></li>          
                      <li><a rel="nofollow"  class="tips" data-title="Linkedin" href="http://www.linkedin.com/shareArticle?mini=true&amp;url='.$permalink.'&amp;title='.$title.'" title=""><i class="icon-linkedin"></i></a></li>          
                      <li><a rel="nofollow"  class="tips" data-title="Delicious" href="http://delicious.com/post?url='.$permalink.'&amp;title='.$title.'" title=""><i class="icon-delicious"></i></a></li>          
                      <li><a rel="nofollow"  class="tips" data-title="Email" href="mailto:?subject='.$title.'&amp;body='.$permalink.'" title=""><i class="icon-mail"></i></a></li>       
                    </ul>
                  </div>';
}

function related_posts() { 
  $orig_post = $post;
  global $post;
  $tags = wp_get_post_tags($post->ID);
  if ($tags) {
    $tag_ids = array();
    foreach($tags as $individual_tag)
      $tag_ids[] = $individual_tag->term_id;
    $args=array(
      'tag__in' => $tag_ids,
      'post__not_in' => array($post->ID),
      'posts_per_page'=>5
      );
    $my_query = new WP_Query( $args );
    if( $my_query->have_posts() ) {
      echo '<div id="relatedpages"><h3>Veja também</h3><ul>';
      while( $my_query->have_posts() ) {
        $my_query->the_post(); ?>
        <li><div class="relatedthumb"><a href="<?php the_permalink()?>" rel="bookmark" title="<?php the_title(); ?>">
          <?php if ( has_post_thumbnail() ) {?>
          <?php the_post_thumbnail('thumbnail'); ?>
       <?php }else{ ?>
         <img src="<?php bloginfo('template_url'); ?>/img/dist/logo-thumb.png" />
       <?php } ?>
          </a></div>
          <div class="relatedcontent">
            <h3><a href="<?php the_permalink()?>" rel="bookmark" title="<?php the_title(); ?>"><?php the_title(); ?></a></h3>
            <span class="date">
              <?php the_time('j \d\e F, Y'); ?>
            </span>
          </div>
        </li>
        <?php }
        echo '</ul></div>';
      } else { 
        echo "Nenhum post relacionado encontrado";
      }
    }
    $post = $orig_post;
    wp_reset_query(); 
  }