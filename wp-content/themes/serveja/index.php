<?php get_header(); ?>
  <body>
   <?php get_template_part( 'nav' );?>
    <div class="row">
      <section class="large-8 columns">

        <?php if (have_posts()) {
                while (have_posts()) { 
                  the_post(); 
                    get_template_part( 'content', get_post_format() );?>
        <?php } 
            }else{ ?>
          <h2>Nenhum post</h2>
        <?php } ?>
        <div class="pagination box">
          <span class="current ">1</span>
          <a href="http://www.bluthemes.com/themes/breeze/page/2" class="inactive ">2</a>
          <a href="http://www.bluthemes.com/themes/breeze/page/2">Next Page <i class="icon-right-open-1"></i></a>
        </div>
      </section>
    <?php get_sidebar(); ?>
    </div>
<?php get_footer(); ?>