module.exports = function(grunt) {

    // 1. load all grunt tasks matching the `grunt-*` pattern
    require('load-grunt-tasks')(grunt);

    require('time-grunt')(grunt);    

    // 2. All configuration goes here 
    grunt.initConfig({
        sass: {
            dist: {
                options: {
                    style: 'compressed'
                },
                files: {
                    'css/dist/style.min.css': 'css/src/style.scss'
                }
            },
            dev: {
                options: {
                    style: 'expanded',
                    lineNumbers: true
                },
                files: {
                    'css/dist/style.css': 'css/src/style.scss'
                }
            },
            editor:{
                options: {
                    style: 'expanded',
                    lineNumbers: true
                },
                files: {
                    'css/dist/editor-style.css': 'css/src/modules/editor-style.scss'
                }
            } 
        },
        concat: {
          dist: {
            src: ['js/src/vendor/jquery.js',
                  'js/src/vendor/fastclick.js',
                  'js/src/vendor/jquery.autofix_anything.js',
                  'js/src/foundation.min.js',
                  'js/src/app.js'],
            dest: 'js/dist/main.js',
          },
        },
        uglify: {
          dev: {
            files: {
              'js/dist/main.min.js': ['js/dist/main.js']
            }
          }
        },
        imagemin: {
          dynamic: {
            options: {
              optimizationLevel: 7,
              progressive: true
            },                        // Another target
            files: [{
              expand: true,                  // Enable dynamic expansion
              cwd: 'img/src/',                   // Src matches are relative to this path
              src: ['**/*.{png,jpg,gif}'],   // Actual patterns to match
              dest: 'img/dist/'                  // Destination path prefix
            }]
          }
        },
        includereplace: {
          html: {
            src: '*.html',
            dest: 'dist/'
          }
        },
        copy: {
          main: {
            files: [
              {expand: true, flatten: true, src: ['js/modernizr.js'], dest: 'dist/js/', filter: 'isFile'}
            ]
          },
        },
        watch: {
          options: {
            livereload: true,
          },
          css: {
            files: 'css/src/**/*.scss',
            tasks: ['sass:dev','sass:editor'],
            
          },
          img: {
            files: 'img/src/*.{png,jpg,gif}',
            tasks: ['imagemin'],
          },
          js: {
            files: 'js/src/*.js',
            tasks:['concat']
          },
          htmlphp: {
            files: ['*.html', '*.php']
          }
        }
    });

    // 4. Where we tell Grunt what to do when we type "grunt" into the terminal.
    grunt.registerTask('prod', ['sass:dev','sass:dist', 'sass:editor','concat','uglify','imagemin']);

    grunt.registerTask('default', ['watch']);

};

