<?php get_header(); ?>
<body>
 <?php get_template_part( 'nav' );?>
 <div class="row main">
  <section class="large-8 columns">

    <?php 
    while (have_posts()) { 
      the_post(); 

      get_template_part( 'content', get_post_format() );?>
      <div class="related-post large-12 columns box">
          <?php related_posts(); ?>
      </div>
      <div class="author-post large-12 columns">
        <div class="large-3 columns">
        <?php echo get_avatar(get_the_author_meta('email'), $size = '96', $default = '<path_to_url>' ); ?>
        </div>
        <div class="author-body large-9 columns">
          <h2 class="vcard author">
            <a href="#" title="Post por <?php the_author_meta('first_name'); ?> <?php the_author_meta('last_name'); ?>" rel="author"><?php the_author_meta('first_name'); ?>  <?php the_author_meta('last_name'); ?></a>
          </h2>
          <p>
            <?php the_author_meta('description'); ?>
          </p>
        </div>
      </div>

      

      <?php } ?>
    </section>
    <?php get_sidebar(); ?>
  </div>
  <?php get_footer(); ?>

