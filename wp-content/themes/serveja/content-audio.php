 <article <?php post_class() ?> id="post-<?php the_ID(); ?>">
 		 <div class="flex-video">
          <?php echo get_post_meta($id,'_format_audio_embed')[0];?>
         </div> 
          <div class="entry-container box">
            <header>
              <h1><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h1>
                <ul class="meta">
                  <li>por <?php the_author_meta('first_name'); ?> <?php the_author_meta('last_name'); ?></p></li>
                  <li class="divider">/</li>
                  <li><time class="entry-date updated" datetime="2013-12-04T13:54:34+00:00"><?php the_time('j \d\e F, Y'); ?></time></li>
                  <li class="divider">/</li>
                  <li><?php the_category(', '); ?> </li>
                  <li class="divider">/</li>
                  <li><a rel="nofollow"  class="tips" data-title="Facebook" href="http://www.facebook.com/sharer.php?u=<?php the_permalink(); ?>&amp;t=<?php the_title(); ?>" title=""><i class="icon-facebook"></i></a></li>
                  <li><a rel="nofollow"  class="tips" data-title="Google+" href="https://plus.google.com/share?url=<?php the_permalink(); ?>title=<?php the_title(); ?>"><i class="icon-googleplus"></i></a></li>
                  <li><a rel="nofollow"  class="tips" data-title="Twitter" href="http://twitter.com/intent/tweet?url=<?php the_permalink(); ?>&amp;text=<?php the_title(); ?>" title=""><i class="icon-twitter"></i></a></li>
                </ul>
            </header>
            <div class="entry-content">
              <?php the_content('Read More'); ?>
            </div>
            <footer class="clearfix">
              <?php share_social_media(get_permalink(), urlencode(get_the_title($id))); ?>
            </footer>
          </div>
        </article>