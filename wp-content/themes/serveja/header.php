<!doctype html>
<html class="no-js" lang="pt-br">
  <head prefix="og: http://ogp.me/ns#">
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <meta name="googlebot" content="index,archive,follow" />
	<meta name="msnbot" content="all,index,follow" />
	<meta name="robots" content="all,index,follow" />
	<meta name="description" content="<?php bloginfo('description'); ?>">

    	<title>
		<?php if (function_exists('is_tag') && is_tag()) {
		single_tag_title('Tag Archive for &quot;'); 
		} elseif (is_archive()) {
		wp_title(''); echo ' Archive - ';
		} elseif (is_search()) {
		echo 'Search for &quot;'.wp_specialchars($s).'&quot; - ';
		} elseif (!(is_404()) && (is_single()) || (is_page())) {
		wp_title(''); echo ' - ';
		} elseif (is_404()) {
		echo 'Not Found - ';
		}
		if (is_home()) {
		bloginfo('name'); echo ' - '; bloginfo('description');
		} else {
		bloginfo('name');
		}
		if ($paged > 1) {
		echo ' - page '. $paged;
		} ?>
		</title>
	<link rel="icon" type="image/png" href="<?php bloginfo('template_url'); ?>/img/dist/favicon.png" />
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/dist/style.css" />
    <script src="<?php bloginfo('template_url'); ?>/js/dist/modernizr.js"></script>
    <link href='http://fonts.googleapis.com/css?family=Droid+Sans:400,700|Roboto+Slab:400,700' rel='stylesheet' type='text/css'>
  	<?php wp_head(); ?>
  </head>
  <div id="fb-root"></div>
	<script>(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/pt_BR/all.js#xfbml=1&appId=1378088989085618";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));</script>
